import sys
from vispy import scene
from vispy import app
from vispy.io import load_data_file, read_png
import numpy as np
from IPython import embed
import tensorflow.keras as keras
from tensorflow.keras.models import load_model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras import backend as K
from pathlib import Path
import matplotlib.pyplot as plt


def run_network(model_name='keras_mnist.h5', batch_size=128,
                epochs=8):

    if not Path(model_name).exists():

        # the data, split between train and test sets
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        img_width = x_test[0].shape[0]
        img_height = x_test[0].shape[1]
        num_classes = len(set(y_train))

        x_train = x_train.reshape(len(x_train), img_width * img_height)
        x_test = x_test.reshape(len(x_test), img_width * img_height)
        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')

        # convert class vectors to binary class matrices
        y_train_one_hot = keras.utils.to_categorical(y_train, num_classes)
        y_test_one_hot = keras.utils.to_categorical(y_test, num_classes)

        model = Sequential()
        model.add(Dense(512, activation='relu', input_shape=(784,)))
        model.add(Dropout(0.5))
        model.add(Dense(512, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(num_classes, activation='softmax', name='output'))

        model.summary()

        model.compile(loss='categorical_crossentropy',
                      optimizer=RMSprop(),
                      metrics=['accuracy'])

        model.fit(x_train, y_train_one_hot,
                  batch_size=batch_size,
                  epochs=epochs,
                  verbose=1,
                  validation_split=.33)

        score = model.evaluate(x_test, y_test_one_hot, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        model.save(model_name)
    else:
        model = load_model(model_name)
    return model


def activation_maximization(model, input_img, output_neuron, step, N, layer_name='output'):
    input_img = input_img.copy()
    img_width = input_img.shape[0]
    img_height = input_img.shape[1]

    input_img = input_img.reshape(1, img_width * img_height)

    layer_dict = dict([(layer.name, layer) for layer in model.layers])
    layer_output = layer_dict[layer_name].output

    loss = layer_output[0][output_neuron]
    all_loss = layer_output[0]

    # compute the gradient of the input picture wrt this loss
    grads = K.gradients(loss, model.input)[0]

    # normalization trick: we normalize the gradient
    grads /= (K.sqrt(K.mean(K.square(grads))) + 1e-15)

    # this function returns the loss and grads given the input picture
    iterate = K.function([model.input], [all_loss, grads])

    # run gradient ascent for 20 steps
    step = 10**step
    for i in range(N):
        loss_value, grads_value = iterate(input_img)
        input_img += grads_value * step

    loss_value, grads_value = iterate(input_img)
    print(np.around(loss_value, 4))
    return input_img.reshape(img_width, img_height)


def main():
    global c, s
    c = 0
    s = 0
    model = run_network()
    img_width = 28
    img_height = 28
    input_img_random = np.random.random((img_width, img_height)) * 0.01 + 0.5
    input_img = activation_maximization(model, input_img_random, 0, -1, 50)
    canvas = scene.SceneCanvas(keys='interactive')
    canvas.size = 800, 600
    canvas.show()

    # Set up a viewbox to display the image with interactive pan/zoom
    view = canvas.central_widget.add_view()

    # Create the image
    interpolation = 'nearest'

    image = scene.visuals.Image(input_img, interpolation=interpolation,
                                parent=view.scene, method='subdivide', cmap='grays')

    # Set 2D camera (the camera will scale to the contents in the scene)
    view.camera = scene.PanZoomCamera(aspect=1)
    # flip y-axis to have correct aligment
    view.camera.flip = (0, 1, 0)
    view.camera.set_range()
    # view.camera.zoom(0.1, (250, 200))

    # Implement key presses

    @canvas.events.key_press.connect
    def on_key_press(event):

        global act, c, s
        update = False
        if event.key in ['Up', 'Down']:
            d = ['Down', 'Up'].index(event.key) * 2 - 1
            s += d / 10
            update = True
        if event.key in ['Left', 'Right']:
            d = ['Left', 'Right'].index(event.key) * 2 - 1
            c += d
            c = np.clip(c, 0, 9)
            update = True

        if update:
            print(c, s)
            input_img = activation_maximization(model, input_img_random, c, s, 50)
            image.set_data(input_img)
            canvas.update()

    app.run()


if __name__ == '__main__' and sys.flags.interactive == 0:
    main()
